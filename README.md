# DAAP - Distance plus Attention for Binding Affinity Prediction

Protein-ligand binding affinity plays a pivotal role in drug development, particularly in identifying potential ligands for target disease-related proteins. Accurate affinity predictions can significantly reduce both the time and cost involved in drug development. However, highly precise affinity prediction remains a research challenge. A key to improve affinity prediction is to capture interactions between proteins and ligands effectively. Existing deep-learning-based computational approaches use 3D grids, 4D tensors, molecular graphs, or proximity-based adjacency matrices, which are either resource-intensive or do not directly represent potential interactions. In this paper, we propose using atomic-level distance features and attention mechanisms to capture better specific protein-ligand interactions based on donor-acceptor relations, hydrophobicity, and π-stacking. We argue that distances encompass both short-range direct and long-range indirect interaction effects while attention mechanisms capture levels of interaction effects. On the very well-known CASF-2016 dataset, our proposed method, named Distance plus Attention for Affinity Prediction (DAAP), outperforms existing methods by achieving Correlation Coefficient (R) 0.888, Root Mean Squared Error (RMSE) 1.061, Mean Absolute Error (MAE) 0.827, Standard Deviation (SD) 1.063, and Concordance Index (CI) 0.853. The proposed method also shows substantial improvement, around 3% to 37%, on four other benchmark datasets.


## Environment
conda = 4.8.3 
cuda = 11.6
python = 3.8 and above
pytorch =  1.8.1


## Feature Construction 

Requirements: To run the program, for each protein you need the following files

	- Ligand SMILES feature in ./DAAP/Data/LL_features folder
	- Protein sequence features in ./DAAP/Data/PP_features folder
	- distance feature for protein-ligand interactions i ./DAAP/Data/PLdist folder
	

Protein sequence features
	- Amino acid one-hot encoding details in "Protein Representation" section
	- Seven physicochemical properties need to extracted. Seven physicochemical properties are given below.
	'X' is for non-standard amino acid residues
	
	'A': [-0.350, -0.680, -0.677, -0.171, -0.170, 0.900, -0.476],
        'C': [-0.140, -0.329, -0.359, 0.508, -0.114, -0.652, 0.476],
        'D': [-0.213, -0.417, -0.281, -0.767, -0.900, -0.155, -0.635],
        'E': [-0.230, -0.241, -0.058, -0.696, -0.868, 0.900, -0.582],
        'F': [ 0.363, 0.373, 0.412, 0.646, -0.272, 0.155, 0.318],
        'G': [-0.900, -0.900, -0.900, -0.342, -0.179, -0.900, -0.900],
        'H': [ 0.384, 0.110, 0.138, -0.271, 0.195, -0.031, -0.106],
        'I': [ 0.900, -0.066, -0.009, 0.652, -0.186, 0.155, 0.688],
        'K': [-0.088, 0.066, 0.163, -0.889, 0.727, 0.279, -0.265],
        'L': [ 0.213, -0.066, -0.009, 0.596, -0.186, 0.714, -0.053],
        'M': [ 0.110, 0.066, 0.087, 0.337, -0.262, 0.652, -0.001],
        'N': [-0.213, -0.329, -0.243, -0.674, -0.075, -0.403, -0.529],
        'P': [ 0.247, -0.900, -0.294, 0.055, -0.010, -0.900, 0.106],
        'Q': [-0.230, -0.110, -0.020, -0.464, -0.276, 0.528, -0.371],
        'R': [ 0.105, 0.373, 0.466, -0.900, 0.900, 0.528, -0.371],
        'S': [-0.337, -0.637, -0.544, -0.364, -0.265, -0.466, -0.212],
        'T': [ 0.402, -0.417, -0.321, -0.199, -0.288, -0.403, 0.212],
        'V': [ 0.677, -0.285, -0.232, 0.331, -0.191, -0.031, 0.900],
        'W': [ 0.479, 0.900, 0.900, 0.900, -0.209, 0.279, 0.529],
        'Y': [ 0.363, 0.417, 0.541, 0.188, -0.274, -0.155, 0.476],
        'X': [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

	- To construct HHM features, you need to install HHblits and obtain the necessary sequence databases. You can download HHblits from the GitHub repository: https://github.com/soedinglab/hh-suite. The sequence databases used can be downloaded from http://wwwuser.gwdg.de/~compbiol/uniclust/2020_06/. Make sure to follow the instructions provided in "Protein Representation" section for feature construction.
	
	
Samples features of core2016 test set proteins are in the  ./DAAP/Data/ folder.


## Run & Test

For prediction
  cd daap/scripts
  python prediction.py 
  



## contact

Julia Rahman : julia.rahman@griffithuni.edu.au
M A Hakim Newton : mahakim.newton@newcastle.edu.au

