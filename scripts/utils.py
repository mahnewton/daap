import torch
from torch import nn


class LogCoshLoss(nn.Module):
    def __init__(self):
        super().__init__()

    @staticmethod
    def forward(y_t, y_prime_t):
        ey_t = y_t - y_prime_t
        return torch.mean(torch.log(torch.cosh(ey_t + 1e-12)))
