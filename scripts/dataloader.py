import pickle
from pathlib import Path

import numpy as np
from torch.utils.data import Dataset

from config import LL_LENGTH, PP_LENGTH, PP_FDIM, PL_LENGTH, LL_FEATURE_PATH, PL_FEATURE_PATH, PP_FEATURE_PATH


class CustomDataset(Dataset):
    def __init__(self, pid_path: Path, label_path: Path):

        all_pids: list = np.loadtxt(fname=str(pid_path.absolute()), dtype='str').tolist()
        y_all: list = np.loadtxt(fname=str(label_path.absolute()), dtype='float').tolist()

        self.ll_data = np.zeros((len(all_pids), LL_LENGTH))  
        self.pp_data = np.zeros((len(all_pids), PP_LENGTH, PP_FDIM))
        self.pl_data = np.zeros((len(all_pids), PL_LENGTH))
        self.y_labels = []

        for i, pid in enumerate(all_pids):
            with open(f"{PL_FEATURE_PATH.absolute()}/{pid}_pl_info.pkl", "rb") as dif:
                pl_info = pickle.load(dif)
            if pl_info['bin_dist'].shape[0] > PL_LENGTH:
                self.pl_data[i, :] = pl_info['bin_dist'][:PL_LENGTH]
            else:
                self.pl_data[i, :pl_info['bin_dist'].shape[0]] = pl_info['bin_dist']

            with open(f"{LL_FEATURE_PATH.absolute()}/{pid}_ll_info.pkl", "rb") as dif:
                ll_info = pickle.load(dif)
            self.ll_data[i] = ll_info['smile_features'][:LL_LENGTH]

            with open(f"{PP_FEATURE_PATH.absolute()}/{pid}_pp_info.pkl", "rb") as dif:
                pp_info = pickle.load(dif)
            if pp_info['pp_feature'].shape[0] > PP_LENGTH:
                self.pp_data[i, :, :] = pp_info['pp_feature'][:PP_LENGTH, :]
            else:
                self.pp_data[i, :pp_info['pp_feature'].shape[0], :] = pp_info['pp_feature'][:,:]

            self.y_labels.append(y_all[i])

    def __getitem__(self, idx):
        pp = np.float32(self.pp_data[idx, :])
        pl = np.int32(self.pl_data[idx, :])
        ll = np.int32(self.ll_data[idx, :])
        return (pp, pl, ll), self.y_labels[idx]

    def __len__(self):
        return len(self.y_labels)
