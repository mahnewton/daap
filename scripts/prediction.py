import os

import torch
from torch.utils.data import DataLoader

from config import CHECKPOINT_PATH, TEST_SET_LIST, TEST_LABEL_LIST
from dataloader import CustomDataset
from model import Model

import numpy as np
from scipy import stats
from sklearn.metrics import mean_squared_error, mean_absolute_error
from math import sqrt


def forward_pass(model, x, device):
    model.eval()
    for i in range(len(x)):
        x[i] = x[i].to(device)
    return model(x)


def test(weight):
    weight_file_path = CHECKPOINT_PATH / weight
    device_name = "cuda" if torch.cuda.is_available() else "cpu"
    device = torch.device(device_name)
    model = Model().to(device)
    model.load_state_dict(torch.load(weight_file_path, map_location=device))
    #print(f"Model weights loaded in {device_name}")

    dataloader = DataLoader(
        dataset=CustomDataset(
            pid_path=TEST_SET_LIST,
            label_path=TEST_LABEL_LIST,
        ),
        batch_size=1
    )
    
    y_pred = []
    for x, y in dataloader:
        prediction = forward_pass(model, x, device)
        prediction = prediction.cpu().detach().numpy()
        for value in prediction[0]:
            y_pred.append(round(value, 3))
    
    return y_pred

    

def result(predicted_score):
    actual_score = np.loadtxt(TEST_LABEL_LIST).tolist() 

    ## Pearson correlation coefficient (R)
    r = stats.pearsonr(actual_score, predicted_score)[0]

    ## Mean square error(MSE), Root mean squared error (RMSE)
    mse = mean_squared_error(actual_score, predicted_score)
    rmse = sqrt(mse)

    ## Mean absolute error (MAE)
    mae = mean_absolute_error(actual_score, predicted_score)

    ## standard deviation (SD) from https://github.com/bioinfocqupt/Sfcnn/blob/main/scripts/evaluation.ipynb
    sd = np.sqrt(sum((np.array(actual_score)-np.array(predicted_score))**2)/(len(actual_score)-1))

    def c_index(y_true, y_pred):
        summ = 0
        pair = 0

        for i in range(1, len(y_true)):
            for j in range(0, i):
                pair += 1
                if y_true[i] > y_true[j]:
                    summ += 1 * (y_pred[i] > y_pred[j]) + 0.5 * (y_pred[i] == y_pred[j])
                elif y_true[i] < y_true[j]:
                    summ += 1 * (y_pred[i] < y_pred[j]) + 0.5 * (y_pred[i] == y_pred[j])
                else:
                    pair -= 1

        if pair != 0:
            return summ / pair
        else:
            return 0

    ci =  c_index(actual_score, predicted_score)
    return np.around( r, 3), np.around( rmse, 3), np.around( mae, 3), np.around( mse, 3), np.around( sd, 3), np.around( ci, 3)


if __name__ == "__main__":
    
    print("r, rmse, mae, sd, ci")
    all_y_pred = []
    if os.path.exists(CHECKPOINT_PATH):
        for (root, dirs, files) in os.walk(CHECKPOINT_PATH):
            for weight in files:
                all_y_pred.append(test(weight))
                
    #print("Test started")
    y_pred = np.mean(np.array(all_y_pred), axis = 0)

    r, rmse, mae, mse, sd, ci = result(y_pred)
    print(r, rmse, mae,  sd, ci)
                
